<?php


namespace App\Http\Controllers;
use App\Database\Tables\Astronauts;
use App\Http\Requests\AstronautRequest;
use App\Models\DAL\Astronaut;
use Carbon\Carbon;
use Symfony\Component\HttpFoundation\JsonResponse;


/**
 * Class AstronautController
 * @package App\Http\Controllers
 *
 * @author Pavel Parma <pavelparma@gmail.com>
 */
class AstronautController extends Controller
{
	/**
	 * Get index page.
	 */
	public function index()
	{
		$astronauts = Astronaut::all();

		return view('astronaut.index', compact('astronauts'));
	}

	/**
	 * Store new astronaut..
	 *
	 * @param AstronautRequest $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function store(AstronautRequest $request)
	{
		$astronaut = Astronaut::create($request->only([Astronauts::NAME, Astronauts::SURNAME, Astronauts::BIRTH_DATE, Astronauts::SUPER_POWER]));

		return response()->json($astronaut->toJson());
	}

	/**
	 * Delete astronaut by id.
	 *
	 * @param $id
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function delete($id)
	{
		$astronaut = Astronaut::findOrFail($id);
		$astronaut->delete();

		return response()->json();
	}

	/**
	 * Update astronaut.
	 *
	 * @param $id
	 * @param AstronautRequest $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function update($id, AstronautRequest $request) {

		$astronaut = Astronaut::findOrFail($id);

		$astronaut->update(
			$request->only([Astronauts::NAME, Astronauts::SURNAME, Astronauts::SUPER_POWER, Astronauts::BIRTH_DATE])
		);

		return response()->json($astronaut->toJson());
	}
}