<?php


namespace App\Models\DAL;
use App\Database\Tables\Astronauts;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;


/**
 * Class Astronaut
 *
 * @package App\Models\DAL
 * @author Pavel Parma <pavelparma@gmail.com>
 * @property integer $id
 * @property string $name
 * @property string $surname
 * @property string $birth_date
 * @property string $super_power
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DAL\Astronaut whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DAL\Astronaut whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DAL\Astronaut whereSurname($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DAL\Astronaut whereBirthDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DAL\Astronaut whereSuperPower($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DAL\Astronaut whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DAL\Astronaut whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Astronaut extends Model
{
	protected $fillable = [
		Astronauts::NAME, Astronauts::SURNAME, Astronauts::BIRTH_DATE, Astronauts::SUPER_POWER
	];
}