/**
 * Add change class into jquery.
 *
 * @param oldClass
 * @param newClass
 * @returns {jQuery}
 */
jQuery.fn.changeClass = function(oldClass, newClass) {
    $(this[0])
        .removeClass(oldClass)
        .addClass(newClass);

    return this;
};

/**
 * Rebind event on element with new callback.
 *
 * @param event
 * @param callback
 * @returns {jQuery}
 */
jQuery.fn.rebind = function(event, callback) {
    $(this[0])
        .unbind(event)
        .on(event, callback);

    return this;
};

/**
 * Add alert to page.
 *
 * @param type
 * @param text
 */
function addAlert(type, text) {
    BootstrapDialog.show({
        type: type,
        message: text,
        buttons: [{
            label: 'Close',
            action: function(dialogItself){
                dialogItself.close();
            },
            cssClass: 'btn-danger'
        }]
    });
}

/**
 * Configure bootstrap style into jquery form validation.
 */
function configureBootstrapFormValidation() {
    $.validator.setDefaults({
        errorClass: "error control-label",
        errorElement: "label",
        errorPlacement: function(error, element) {
            element.closest(".form-group").append(error);
        },
        highlight: function (element, errorClass, validClass) {
            var form_group = $(element).closest('.form-group');

            form_group.removeClass('has-success has-feedback').addClass('has-error has-feedback');
            form_group.closest('.form-group').find('span.glyphicon:not(.calendar)').remove();
            
            if(form_group.find(".input-group-addon").length != 0)
                form_group.find(".input-group-addon").append('<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>');
            else
                form_group.append('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
        },
        unhighlight: function (element, errorClass, validClass) {
            var form_group = $(element).closest('.form-group');

            form_group.removeClass('has-error has-feedback').addClass('has-success has-feedback');
            form_group.find('span.glyphicon:not(.calendar)').remove();

            if(form_group.find(".input-group-addon").length != 0)
                form_group.find(".input-group-addon").append('<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>');
            else
                form_group.append('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
        }
    });
}

/**
 * Configure bootstrap datepicker.
 */
function configureBootstrapDatepicker() {
    $.fn.datepicker.defaults.format = "yyyy-mm-dd";
    $.fn.datepicker.defaults.autoclose = true;
}

/**
 * Mark required inputs.
 */
function markRequiredInputs() {
    $("input[required]").each(function(){
        var label = $(this).closest('.form-group').find('label[for=' + $(this).attr('id') + ']');
        if(label.length != 0)
            $("<span class='required'>*</span>").insertAfter(label);
    });
}

/**
 * Change cell text into input instead.
 *
 * @param cell
 * @param input
 */
function changeCellTextToInput(cell, input) {
    input = $(input)
        .val($(cell).text())
        .attr('data-origin-val', $(cell).text());

    var inputGroup = $('<div class="form-group"></div>').prepend(input);

    $(cell)
        .empty()
        .prepend(inputGroup);
}

/**
 * Change cell input into text instead.
 *
 * @param cell
 * @param useOriginalValue
 */
function changeCellInputToText(cell, useOriginalValue = false) {
    var input = $(cell).find("input");
    $(cell)
        .empty()
        .text(useOriginalValue ? input.attr('data-origin-val') : input.val());
}