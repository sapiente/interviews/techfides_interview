<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" type="image/x-icon" href="http://team.techfides.cz/wp-content/themes/techfides/images/favicon_ico.ico">
    <title>Astronauts IS</title>
    <link type="text/css" rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('css/app.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('css/welcome.css')}}">
</head>
<body>
    <main class="">
        <div class="background-image"></div>
        <div class="content">
            <button><a href="{{action('AstronautController@index')}}">Continue</a></button>
            <button><a href="#">Log in</a></button>
        </div>
    </main>
</body>
</html>
