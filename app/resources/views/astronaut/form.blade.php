<div class="form-group">
    <label for="name" class="control-label">Name</label>
    <input type="text" class="form-control" id="name" name="name" required>
</div>

<div class="form-group">
    <label for="surname" class="control-label">Surname</label>
    <input type="text" class="form-control" id="surname" name="surname" required>
</div>

<div class="form-group">
    <label for="birth_date" class="control-label">Birth date</label>
    {{--<input type="date" class="form-control" id="birth_date" name="birth_date" required>--}}
    <div class="input-group date" data-provide="datepicker">
        <input type="text" class="form-control" name="birth_date" id="birth_date" required>
        <div class="input-group-addon">
            <span class="calendar glyphicon glyphicon-th"></span>
        </div>
    </div>
</div>

<div class="form-group">
    <label for="super_power" class="control-label">Super power</label>
    <input type="text" class="form-control" id="super_power" name="super_power" required>
</div>