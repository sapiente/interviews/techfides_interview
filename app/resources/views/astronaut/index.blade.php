@extends('shared.layout')

@section('content')
    <div id="info"></div>
    <button style="margin-bottom: 10px" type="button" class="btn btn-success center-block" data-toggle="modal" data-target="#myModal">
        New &nbsp; <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
    </button>

    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <form id="create-form">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">New astronaut</h4>
                        <div id="modal-info"></div>
                    </div>
                    <div class="modal-body">
                        @include('astronaut.form')
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Create</button>
                        <button id="close-model" type="button" class="btn btn-danger" data-dismiss="modal">Back</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
    <form id="main-form">
    <table class="astronauts-table table table-hover table-responsive">
        <thead>
            <tr>
                <th>Name</th>
                <th>Surname</th>
                <th>Birth date</th>
                <th>Super power</th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        @foreach($astronauts as $astronaut)
            <tr data-id="{{$astronaut->id}}">
                <td data-input-name="name">{{$astronaut->name}}</td>
                <td data-input-name="surname">{{$astronaut->surname}}</td>
                <td data-input-name="birth_date" data-datepicker="true">{{$astronaut->birth_date}}</td>
                <td data-input-name="super_power">{{$astronaut->super_power}}</td>
                <td>
                    <button data-intent="edit" type="button" class="btn btn-primary">
                        <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                    </button>
                </td>
                <td>
                    <button data-intent="delete" type="button" class="btn btn-danger">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    </form>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {

            //configure delete action
            $("button[data-intent=delete]").click(function(){
                deleteModel($(this).closest("tr"))
            });

            $("button[data-intent=edit]").click(function(){
                var tr = $(this).closest("tr");
                editModel(tr);
            });

            markRequiredInputs();

            //configure datepicker
            configureBootstrapDatepicker();
            $('.datepicker').datepicker({});
            //additional input validation due to datepicker
            $("#birth_date").change(function(){
                $("#create-form").validate().element('#birth_date');
            });

            //configure jquery validation and create action
            configureBootstrapFormValidation();
            $("#create-form").validate({
                submitHandler: function() {
                    $.ajax({
                        method: "post",
                        url: '{{url('/api/astronaut/new')}}',
                        data: $("#create-form").serialize()
                    }).done(function (response) {
                        $("#create-form")[0].reset();
                        $("#close-model").trigger("click");
                        insertNewModel(JSON.parse(response));
                        addAlert(BootstrapDialog.TYPE_SUCCESS, "Creating success.")
                    }).fail(function (response) {
                        if(response.status === 422) {
                            addAlert(BootstrapDialog.TYPE_DANGER, "Creating failed. Please check your inputs.")
                        } else {
                            addAlert(BootstrapDialog.TYPE_DANGER, "Creating failed.")
                        }
                    });

                    return false;
                }
            });
        });

        function deleteModel(row) {
            var url = '{{url('/')}}' + '/api/astronaut/' + row.attr('data-id');

            $.ajax({
                method: "delete",
                url: url
            }).done(function () {
                row.hide(1200, function(){
                    $(this).remove();
                });

                addAlert(BootstrapDialog.TYPE_SUCCESS, "Astronaut successfully removed.")

            }).fail(function () {
                addAlert(BootstrapDialog.TYPE_DANGER, "Removing failed.")
            });
        }

        function editModel(row) {

            row.find('td[data-input-name]').each(function(){
                var name = $(this).attr('data-input-name');
                changeCellTextToInput(this, '<input type="text" name="'+ name +'" id="'+(name + row.attr('data-id')) +'" class="form-control">');
            });

            setDatePickers(row);

            setModelInputsRules(row);
            $("#main-form").validate();

            $('<span data-intent="cancel">Cancel</span>').insertBefore(row.find('td:last-child span.glyphicon'));
            row.find('td:last-child span.glyphicon').hide();

            var saveButtonCell = row.find('td').eq(-2);
            $('<span data-intent="save">Save</span>').insertBefore(saveButtonCell.find('span.glyphicon'));
            saveButtonCell.find('span.glyphicon').hide();
            saveButtonCell.find('button').changeClass('btn-primary', 'btn-success');

            row.find('button[data-intent=delete]').rebind('click', function(){
                finishEditedModel(row, true);
            });

            row.find('button[data-intent=edit]').rebind('click', function(){
                saveEditedModel(row);
            });
        }

        function saveEditedModel(row) {
            var url = '{{url('/')}}' + '/api/astronaut/' + row.attr('data-id');

            $.ajax({
                method: "put",
                url: url,
                data: getInputDataInsideRow(row)
            }).done(function () {
                addAlert(BootstrapDialog.TYPE_SUCCESS, "Astronaut successfully edited.");
                finishEditedModel(row, false);
            }).fail(function () {
                addAlert(BootstrapDialog.TYPE_DANGER, "Editing failed.");
                finishEditedModel(row, true);
            });
        }

        /**
         * Get input data - model data, inside row.
         */
        function getInputDataInsideRow(row) {
            var data = '';
            row.find("input[name]").each(function(){
                data += ('&' + $(this).attr('name') + '=' + $(this).val() + '');
            });

            if(data.startsWith('&'))
                data = data.substr(1);

            return data;
        }

        /**
         * Set datepickers to date inputs.
         */
        function setDatePickers(row) {
            row.find('td[data-datepicker] input').each(function(){
                var input = $(this);
                var group = input.closest('.form-group');
                var newContent = $(
                        '<div class="input-group date" data-provide="datepicker">' +
                            '<div class="input-group-addon">' +
                                '<span class="calendar glyphicon glyphicon-th"></span>' +
                            '</div>' +
                        '</div>'
                );
                input.prependTo(newContent);
                group.append(newContent);
            });
        }

        /**
         * Set model inputs their rules.
         */
        function setModelInputsRules(row) {
            row.find('input[name]').attr('required', true);

            row.find('input[date]').attr('required', true);
        }

        /**
         * Finish edited model.
         */
        function finishEditedModel(row, useOriginValues = false) {
            row.find("input[name]").each(function(){
                changeCellInputToText($(this).closest('td'), useOriginValues);
            });

            row.find('td:last-child span.glyphicon').show();
            row.find('td:last-child span[data-intent=cancel]').remove();

            var saveButtonCell = row.find('td').eq(-2);
            saveButtonCell.find('span.glyphicon').show();
            saveButtonCell.find('span[data-intent=save]').remove();
            saveButtonCell.find('button').changeClass('btn-success', 'btn-primary');

            row.find('button[data-intent=delete]').rebind('click', function(){
                deleteModel(row);
            });

            row.find('button[data-intent=edit]').rebind('click', function(){
                editModel(row);
            });
        }

        /**
         * Insert new model into table.
         *
         * @param model
         */
        function insertNewModel(model) {
            $(".astronauts-table tbody").append(
                    '<tr data-id="' + model.id +'">' +
                        '<td data-input-name="name">'+ model.name +"</td>" +
                        '<td data-input-name="surname">'+ model.surname +"</td>" +
                        '<td data-input-name="birth_date" data-datepicker="true">'+ model.birth_date +"</td>" +
                        '<td data-input-name="super_power">'+ model.super_power +"</td>" +
                        "<td>" +
                            '<button data-intent="edit" type="button" class="btn btn-primary">' +
                                '<span class="glyphicon glyphicon-edit" aria-hidden="true"></span>'+
                            '</button>' +
                        '</td>' +
                        '<td>' +
                            '<button data-intent="delete" type="button" class="btn btn-danger"> ' +
                                '<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>' +
                            '</button>' +
                        "</td>"+
                    "</tr>"
            );

            $("tr[data-id=" + model.id+"] button[data-intent=delete]").click(function(){
                deleteModel($(this).closest("tr"))
            });

            $("tr[data-id=" + model.id+"] button[data-intent=edit]").click(function(){
                var tr = $(this).closest("tr");
                editModel(tr);
            });
        }
    </script>
@endsection