<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" type="image/x-icon" href="http://team.techfides.cz/wp-content/themes/techfides/images/favicon_ico.ico">
    <title>Astronauts IS</title>
    <link type="text/css" rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('css/bootstrap-dialog.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('css/app.css')}}">
    <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.standalone.css">
    {{--<link type="text/css" rel="stylesheet" href="{{asset("css/bootstrap-datepicker3.standalone.min.css")}}">--}}
    @yield('links')
</head>
<body>
<div class="flex-center position-ref full-height">
    @include('shared.nav')
    <main class="container">
        @yield('content')
    </main>
    @include('shared.footer')
    <script src="{{asset('js/jquery-3.1.1.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/jquery.validate.min.js')}}"></script>
    <script src="{{asset('js/jquery-validation-additional-methods.min.js')}}"></script>
{{--    <script src="{{asset('js/bootstrap-datepicker.min.js')}}"></script>--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
    <script src="{{asset('js/bootstrap-dialog.js')}}"></script>
    <script src="{{asset('js/app.js')}}"></script>
    @yield('scripts')
</div>
</body>
</html>
