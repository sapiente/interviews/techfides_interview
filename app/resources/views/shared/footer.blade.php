<footer class="footer text-center">
    <div class="container">
        <p class="text-muted">Created by <a target="_blank" href="https://www.facebook.com/pavel.parma">@@slythe</a> for <a target="_blank" href="http://www.techfides.cz/">TechFides</a></p>
    </div>
</footer>