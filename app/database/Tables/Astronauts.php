<?php


namespace App\Database\Tables;


/**
 * Class Astronauts
 * @package App\Database\Tables
 *
 * @author Pavel Parma <pavelparma@gmail.com>
 */
class Astronauts
{
	const TABLE = 'astronauts';

	const
		ID = 'id',
		NAME = 'name',
		SURNAME = 'surname',
		BIRTH_DATE = 'birth_date',
		SUPER_POWER = 'super_power';
}