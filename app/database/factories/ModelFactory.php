<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Database\Tables\Astronauts;

$factory->define(\App\Models\DAL\Astronaut::class, function (Faker\Generator $faker) {

	return [
		Astronauts::NAME => $faker->name,
		Astronauts::SURNAME => $faker->name,
		Astronauts::BIRTH_DATE => $faker->date(),
		Astronauts::SUPER_POWER => str_random(10),
	];
});
