<?php

use App\Database\Tables\Astronauts;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAstronautsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create(Astronauts::TABLE, function(Blueprint $table) {
			$table->increments(Astronauts::ID);
			$table->string(Astronauts::NAME, 50);
			$table->string(Astronauts::SURNAME, 50);
			$table->date(Astronauts::BIRTH_DATE);
			$table->string(Astronauts::SUPER_POWER);
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Astronauts::TABLE);
    }
}
